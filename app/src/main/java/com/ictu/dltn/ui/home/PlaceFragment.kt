package com.ictu.dltn.ui.home

import com.ictu.dltn.utils.Const

class PlaceFragment : PostFragment() {

    override fun getPostType(): Int {
        return Const.POST_TYPE_PLACE
    }
}