package com.ictu.dltn.ui.user

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codepath.asynchttpclient.AsyncHttpClient
import com.codepath.asynchttpclient.RequestHeaders
import com.codepath.asynchttpclient.RequestParams
import com.codepath.asynchttpclient.callback.TextHttpResponseHandler
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ictu.dltn.models.BaseResponse
import com.ictu.dltn.models.User
import com.ictu.dltn.utils.Const
import com.ictu.dltn.utils.Db
import com.ictu.dltn.utils.Utils
import okhttp3.Headers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import timber.log.Timber
import java.lang.reflect.Type

class UserModel : ViewModel() {
    private val _user = MutableLiveData<User?>()
    val user: LiveData<User?> = _user

    fun getUser(context: Context) {
        _user.value = Db.getUser(context)
    }

    fun logout(context: Context) {
        _user.value = null
        Db.saveUser(context, null)
    }

    fun login(phone: String, pass: String, onFinish: (User?) -> Unit) {
        val passMd5 = Utils.md5(pass)

        val client = AsyncHttpClient()
        val requestHeaders = RequestHeaders()
        val params = RequestParams()
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("phone", phone)
            .addFormDataPart("pass", passMd5)
            .build()

        client.post(
            Const.urlLogin(),
            requestHeaders,
            params,
            requestBody,
            object : TextHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Headers?, response: String) {
                    val type: Type = object : TypeToken<BaseResponse<User>>() {}.type
                    val json = Gson().fromJson<BaseResponse<User>>(response, type)
                    _user.value = json.data
                    onFinish.invoke(_user.value)
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Headers?,
                    response: String?,
                    throwable: Throwable?
                ) {
                    Timber.e("error:${throwable.toString()}")
                    onFinish.invoke(null)
                }
            })
    }

    fun register(phone: String, pass: String, name:String, onFinish: (User?) -> Unit) {
        val passMd5 = Utils.md5(pass)

        val client = AsyncHttpClient()
        val requestHeaders = RequestHeaders()
        val params = RequestParams()
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("phone", phone)
            .addFormDataPart("pass", passMd5)
            .addFormDataPart("name", name)
            .build()

        client.post(
            Const.urlRegister(),
            requestHeaders,
            params,
            requestBody,
            object : TextHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Headers?, response: String) {
                    Timber.e("ok $response")
                    val type: Type = object : TypeToken<BaseResponse<User>>() {}.type
                    val json = Gson().fromJson<BaseResponse<User>>(response, type)
                    _user.value = json.data
                    onFinish.invoke(_user.value)
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Headers?,
                    response: String?,
                    throwable: Throwable?
                ) {
                    Timber.e("error:${throwable.toString()}")
                    onFinish.invoke(null)
                }
            })
    }
}