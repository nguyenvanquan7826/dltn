package com.ictu.dltn.ui.rate

import androidx.lifecycle.ViewModel
import com.codepath.asynchttpclient.AsyncHttpClient
import com.codepath.asynchttpclient.RequestHeaders
import com.codepath.asynchttpclient.RequestParams
import com.codepath.asynchttpclient.callback.TextHttpResponseHandler
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ictu.dltn.models.BaseResponse
import com.ictu.dltn.utils.Const
import okhttp3.Headers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import timber.log.Timber
import java.lang.reflect.Type

class RateModel : ViewModel() {

    fun rate(phone: String, postId: Int, rate: Int, content: String, onFinish: (Boolean) -> Unit) {
        val client = AsyncHttpClient()
        val requestHeaders = RequestHeaders()
        val params = RequestParams()
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("phone", phone)
            .addFormDataPart("post_id", postId.toString())
            .addFormDataPart("rate", rate.toString())
            .addFormDataPart("comment", content)
            .build()

        client.post(
            Const.urlAddRate(),
            requestHeaders,
            params,
            requestBody,
            object : TextHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Headers?, response: String) {
                    Timber.e("success $response")
                    val type: Type = object : TypeToken<BaseResponse<String>>() {}.type
                    val json = Gson().fromJson<BaseResponse<String>>(response, type)
                    onFinish.invoke(json.status ?: false)
                }

                override fun onFailure(
                    statusCode: Int,
                    headers: Headers?,
                    response: String?,
                    throwable: Throwable?
                ) {
                    Timber.e("error:${throwable.toString()}")
                    onFinish.invoke(false)
                }
            })
    }
}