package com.ictu.dltn.ui.user

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.ictu.dltn.R
import com.ictu.dltn.databinding.FragmentLoginBinding
import com.ictu.dltn.models.User
import com.ictu.dltn.utils.Db
import com.ictu.dltn.utils.Utils

class LoginFragment : Fragment() {

    private lateinit var viewModel: UserModel
    private var _binding: FragmentLoginBinding? = null

    private val binding get() = _binding!!
    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        user = Db.getUser(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            )[UserModel::class.java]

        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.edtPass.background = Utils.getDrawableEdt(requireContext())
        binding.edtPhone.background = Utils.getDrawableEdt(requireContext())
        binding.btnLogin.background = Utils.getDrawableBtn(requireContext())

        binding.edtPhone.setText("0987654321")
        binding.edtPass.setText("a")

        binding.btnLogin.setOnClickListener {
            viewModel.login(binding.edtPhone.text.toString(), binding.edtPass.text.toString()) {
                if (it != null) {
                    Db.saveUser(requireContext(), it)
                    requireActivity().onBackPressed()
                } else {
                    Toast.makeText(requireContext(), R.string.warong_user_passs, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

        binding.btnRegister.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}