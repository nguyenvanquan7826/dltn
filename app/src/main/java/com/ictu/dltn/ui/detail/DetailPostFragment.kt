package com.ictu.dltn.ui.detail

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.ictu.dltn.R
import com.ictu.dltn.databinding.FragmentDetailPostBinding
import com.ictu.dltn.models.Post
import com.ictu.dltn.models.Rate
import com.ictu.dltn.models.User
import com.ictu.dltn.ui.adapter.PostNearAdapter
import com.ictu.dltn.ui.adapter.RateAdapter
import com.ictu.dltn.utils.Const
import com.ictu.dltn.utils.Db
import com.ictu.dltn.utils.Utils
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView

class DetailPostFragment : Fragment() {

    private var _binding: FragmentDetailPostBinding? = null
    private lateinit var viewModel: DetailPostViewModel
    private val binding get() = _binding!!
    private var user: User? = null
    private val args: DetailPostFragmentArgs by navArgs()
    private lateinit var post: Post

    private val rateAdapter = RateAdapter(arrayListOf()) {
    }

    private val postNearAdapter = PostNearAdapter(arrayListOf()) {
        findNavController().navigate(
            DetailPostFragmentDirections.actionDetailPostFragmentToDetailPostFragment(it)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        post = args.post
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailPostBinding.inflate(inflater, container, false)


        viewModel = ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        )[DetailPostViewModel::class.java]

        viewModel.listRate.observe(viewLifecycleOwner, { onChangeListRate(it) })

        viewModel.listPostNear.observe(viewLifecycleOwner, { onChangeListRestaurantNear(it) })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupImageSlider()
        binding.tvContent.text = post.content
        binding.tvTitle.text = post.name?.trim()
        binding.refreshLayout.setOnRefreshListener { reloadData() }
        binding.tvAddress.text = post.address
        binding.layoutRestaurantNear.visibility =
            if (post.type == Const.POST_TYPE_PLACE) View.VISIBLE else View.GONE

        binding.tvAddress.setOnClickListener { Utils.openMap(requireContext(), post.lat, post.lng) }
        binding.tvClickRating.setOnClickListener { onClickRate() }
    }

    private fun setupImageSlider() {
        val sliderView = binding.imageSlider

        val adapter = SliderAdapterExample(requireContext())
        adapter.renewItems(args.post.images)

        sliderView.setSliderAdapter(adapter)

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM) //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!

        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = Color.WHITE
        sliderView.indicatorUnselectedColor = Color.GRAY
        sliderView.scrollTimeInSec = 4 //set scroll delay in seconds :

        sliderView.startAutoCycle()
    }

    private fun onChangeListRate(list: List<Rate>) {
        binding.refreshLayout.isRefreshing = false

        val ratingAverage = viewModel.getRateAverage()
        val total = viewModel.getListSize()
        binding.tvNumUserRate.text = total.toString()
        binding.tvRateValue.text = ratingAverage.toString()
        binding.ratingBarTitle.rating = ratingAverage.toFloat()
        binding.ratingBarAverage.rating = ratingAverage.toFloat()
        binding.ratingBar1.progress = viewModel.countRate[1]
        binding.ratingBar2.progress = viewModel.countRate[2]
        binding.ratingBar3.progress = viewModel.countRate[3]
        binding.ratingBar4.progress = viewModel.countRate[4]
        binding.ratingBar5.progress = viewModel.countRate[5]

        rateAdapter.setData(list)
        binding.rvRate.apply {
            adapter = rateAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun onChangeListRestaurantNear(list: List<Post>) {
        binding.tvNoRestaurantNear.visibility = if (list.isEmpty()) View.VISIBLE else View.GONE
        postNearAdapter.setData(list)
        binding.rvNear.apply {
            adapter = postNearAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }
    }

    private fun onClickRate() {
        if (user == null) {
            findNavController().navigate(
                DetailPostFragmentDirections.actionDetailPostFragmentToLoginFragment()
            )
            return
        }

        if (viewModel.wasRate(user?.phone ?: "")) {
            Toast.makeText(requireContext(), R.string.was_rate, Toast.LENGTH_SHORT).show()
            return
        }

        findNavController().navigate(
            DetailPostFragmentDirections.actionDetailPostFragmentToRateFragment(post)
        )
    }

    private fun reloadData() {
        binding.refreshLayout.isRefreshing = true
        viewModel.getListRate(post.id)
        viewModel.getListPostNear(post)

        user = Db.getUser(requireContext())
    }

    override fun onResume() {
        super.onResume()
        reloadData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}