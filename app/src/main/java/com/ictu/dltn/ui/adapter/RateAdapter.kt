package com.ictu.dltn.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ictu.dltn.R
import com.ictu.dltn.models.Post
import com.ictu.dltn.models.Rate

class RateAdapter(private var data: List<Rate>, val onItemClick: (Post) -> Unit) :
    RecyclerView.Adapter<RateAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(rate: Rate) {
            val tvName = view.findViewById<TextView>(R.id.tvName)
            val tvContent = view.findViewById<TextView>(R.id.tvContent)
            val tvTime = view.findViewById<TextView>(R.id.tvTime)
            val ratingBar = view.findViewById<RatingBar>(R.id.ratingBar)

            tvName.text = rate.userName
            tvContent.text = rate.comment
            tvTime.text = rate.createAt
            ratingBar.rating = rate.rate.toFloat()

            // view.setOnClickListener { onItemClick(rate) }
        }
    }


    private var listFiller: List<Rate> = arrayListOf()

    public fun setData(data: List<Rate>) {
        this.data = data
        listFiller = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_rate, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return listFiller.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(listFiller[position])
    }

}