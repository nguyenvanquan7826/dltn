package com.ictu.dltn.ui.rate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.ictu.dltn.R
import com.ictu.dltn.databinding.FragmentRateBinding
import com.ictu.dltn.models.Post
import com.ictu.dltn.models.User
import com.ictu.dltn.ui.detail.DetailPostFragmentArgs
import com.ictu.dltn.utils.Db
import com.ictu.dltn.utils.Utils

class RateFragment : Fragment() {

    private lateinit var viewModel: RateModel
    private var _binding: FragmentRateBinding? = null

    private val binding get() = _binding!!
    private var user: User? = null

    val args: DetailPostFragmentArgs by navArgs()
    private lateinit var post: Post

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        user = Db.getUser(requireContext())
        post = args.post
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            )[RateModel::class.java]

        _binding = FragmentRateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnRate.background = Utils.getDrawableBtn(requireContext())
        binding.btnRate.setOnClickListener { onSubmitRate() }
    }

    private fun onSubmitRate() {
        val content = binding.edtContent.text.toString()
        val rate = binding.ratingBar.rating.toInt()
        if (content.isEmpty()) {
            Toast.makeText(requireContext(), R.string.rate_please_input_content, Toast.LENGTH_SHORT)
                .show()
            return
        }
        if (rate < 1) {
            Toast.makeText(requireContext(), R.string.rate_please_rate, Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.rate(user?.phone ?: "", post.id, rate, content) {
            if (it) {
                requireActivity().onBackPressed()
            } else {
                Toast.makeText(requireContext(), R.string.rate_fail, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}