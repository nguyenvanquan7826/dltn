package com.ictu.dltn.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ictu.dltn.R
import com.ictu.dltn.databinding.FragmentPlaceBinding
import com.ictu.dltn.ui.adapter.PostAdapter
import com.ictu.dltn.utils.Const
import com.ictu.dltn.utils.Utils
import timber.log.Timber
import top.defaults.drawabletoolbox.DrawableBuilder

open class PostFragment : Fragment() {

    private lateinit var homeViewModel: PostViewModel
    private var _binding: FragmentPlaceBinding? = null

    private val binding get() = _binding!!
    private val postAdapter = PostAdapter(arrayListOf()) {
        when (getPostType()) {
            Const.POST_TYPE_PLACE ->
                findNavController().navigate(PlaceFragmentDirections.actionToDetailPostFragment(it))
            Const.POST_TYPE_RESTAURANT ->
                findNavController().navigate(
                    RestaurantFragmentDirections.actionRestaurantFragmentToDetailPostFragment(it)
                )
        }
    }

    open fun getPostType(): Int {
        return Const.POST_TYPE_PLACE
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPlaceBinding.inflate(inflater, container, false)
        val root: View = binding.root

        homeViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        )[PostViewModel::class.java]

        homeViewModel.listPost.observe(viewLifecycleOwner, {
            postAdapter.setData(it)
            binding.refreshLayout.isRefreshing = false
        })


        binding.rv.apply {
            adapter = postAdapter
            layoutManager = LinearLayoutManager(context)
        }

        binding.refreshLayout.setOnRefreshListener { reloadData() }
        binding.edtSearch.setBackgroundDrawable(Utils.getDrawableEdt(requireContext()))
        binding.edtSearch.addTextChangedListener {
            Timber.e("s:" + it.toString())
            postAdapter.filter.filter(it.toString())
        }


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reloadData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun reloadData() {
        binding.refreshLayout.isRefreshing = true
        homeViewModel.getListPost(getPostType())
    }
}