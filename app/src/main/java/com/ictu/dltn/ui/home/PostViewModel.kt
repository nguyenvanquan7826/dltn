package com.ictu.dltn.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codepath.asynchttpclient.AsyncHttpClient
import com.codepath.asynchttpclient.RequestParams
import com.codepath.asynchttpclient.callback.TextHttpResponseHandler
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ictu.dltn.models.BaseResponse
import com.ictu.dltn.models.Post
import com.ictu.dltn.utils.Const
import okhttp3.Headers
import timber.log.Timber
import java.lang.reflect.Type

class PostViewModel : ViewModel() {
    private val _listPost = MutableLiveData<List<Post>>()
    val listPost: LiveData<List<Post>> = _listPost

    fun getListPost(type: Int) {
        val client = AsyncHttpClient()
        val params = RequestParams()
        client[Const.urlGetListPost(type), params, object :
            TextHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Headers?, response: String) {
                val type: Type = object : TypeToken<BaseResponse<List<Post>>>() {}.type
                val json = Gson().fromJson<BaseResponse<List<Post>>>(response, type)
                _listPost.value = json.data ?: arrayListOf()
            }

            override fun onFailure(
                statusCode: Int,
                headers: Headers?,
                errorResponse: String?,
                t: Throwable?
            ) {
                Timber.e("error:${t.toString()}")
            }
        }]

    }
}