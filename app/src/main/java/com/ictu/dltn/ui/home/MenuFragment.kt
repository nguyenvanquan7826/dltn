package com.ictu.dltn.ui.home

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.ictu.dltn.R
import com.ictu.dltn.databinding.FragmentMenuBinding
import com.ictu.dltn.models.User
import com.ictu.dltn.ui.user.UserModel
import com.ictu.dltn.utils.Const
import com.ictu.dltn.utils.Utils
import timber.log.Timber

class MenuFragment : Fragment() {

    private lateinit var viewModel: UserModel
    private var _binding: FragmentMenuBinding? = null

    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            )[UserModel::class.java]

        _binding = FragmentMenuBinding.inflate(inflater, container, false)

        viewModel.user.observe(viewLifecycleOwner, { onUserChange(it) })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.layoutUser.setOnClickListener { onClickUser() }
        binding.btnLogout.background = Utils.getDrawableBtn(requireContext())
        binding.btnLogout.setOnClickListener { viewModel.logout(requireContext()) }
        binding.refreshLayout.setOnRefreshListener { loadData() }
    }

    private fun onClickUser() {
        if (viewModel.user.value == null) {
            findNavController().navigate(MenuFragmentDirections.actionMenuFragmentToLoginFragment())
        }
    }

    private fun onUserChange(user: User?) {
        val urlAvatar =
            if (!TextUtils.isEmpty(user?.avatar)) user?.avatar else Const.URL_DEFAUL_AVATAR
        Timber.e("avatar $urlAvatar")
        Glide.with(requireContext()).load(urlAvatar).centerCrop().into(binding.ivAvatar)
        binding.tvName.text = user?.name ?: getString(R.string.login)
        binding.tvPhone.text = user?.phone ?: ""
        binding.btnLogout.visibility = if (user == null) View.GONE else View.VISIBLE
        binding.tvPhone.visibility = if (user == null) View.GONE else View.VISIBLE
    }

    private fun loadData() {
        viewModel.getUser(requireContext())
        binding.refreshLayout.isRefreshing = false
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}