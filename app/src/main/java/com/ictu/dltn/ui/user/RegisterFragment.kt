package com.ictu.dltn.ui.user

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.ictu.dltn.R
import com.ictu.dltn.databinding.FragmentRegisterBinding
import com.ictu.dltn.models.User
import com.ictu.dltn.utils.Db
import com.ictu.dltn.utils.Utils

class RegisterFragment : Fragment() {

    private lateinit var viewModel: UserModel
    private var _binding: FragmentRegisterBinding? = null

    private val binding get() = _binding!!
    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        user = Db.getUser(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            )[UserModel::class.java]

        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.edtPhone.background = Utils.getDrawableEdt(requireContext())
        binding.edtPass.background = Utils.getDrawableEdt(requireContext())
        binding.edtRePass.background = Utils.getDrawableEdt(requireContext())
        binding.edtName.background = Utils.getDrawableEdt(requireContext())
        binding.btnRegister.background = Utils.getDrawableBtn(requireContext())

        binding.edtPhone.setText("0987654321")
        binding.edtPass.setText("a")
        binding.edtRePass.setText("a")
        binding.edtName.setText("Nguyen Van Anh")

        binding.btnRegister.setOnClickListener { onClickRegister() }
    }

    private fun onClickRegister() {
        val phone = binding.edtPhone.text.toString()
        val pass = binding.edtPass.text.toString()
        val repass = binding.edtRePass.text.toString()
        val name = binding.edtName.text.toString()

        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(
                requireContext(),
                R.string.register_please_input_phone,
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(
                requireContext(),
                R.string.register_please_input_pass,
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (!TextUtils.equals(pass, repass)) {
            Toast.makeText(
                requireContext(),
                R.string.register_pass_not_same,
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(
                requireContext(),
                R.string.register_please_input_name,
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        viewModel.register(phone, pass, name) {
            if (it != null) {
                Toast.makeText(requireContext(), R.string.register_success, Toast.LENGTH_SHORT)
                    .show()
                Db.saveUser(requireContext(), it)
                requireActivity().onBackPressed()
            } else {
                Toast.makeText(requireContext(), R.string.register_fail, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}