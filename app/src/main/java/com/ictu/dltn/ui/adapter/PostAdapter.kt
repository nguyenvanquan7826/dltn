package com.ictu.dltn.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ictu.dltn.R
import com.ictu.dltn.models.Post
import timber.log.Timber

class PostAdapter(private var data: List<Post>, val onItemClick: (Post) -> Unit) :
    RecyclerView.Adapter<PostAdapter.MyViewHolder>(),
    Filterable {

    inner class MyViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(post: Post) {
            val title = view.findViewById<TextView>(R.id.tvTitle)
            val imageView = view.findViewById<ImageView>(R.id.imageView)
            val description = view.findViewById<TextView>(R.id.tvDescription)
            val ratingBar = view.findViewById<RatingBar>(R.id.ratingBar)

            title.text = post.name?.trim()
            description.text = post.content
            ratingBar.rating = post.rateAverage.toFloat()

            Glide.with(view.context).load(post.getFirstImage()).centerCrop().into(imageView)
            view.setOnClickListener { onItemClick(post) }
        }
    }


    private var listFiller: List<Post> = arrayListOf()

    public fun setData(data: List<Post>) {
        this.data = data
        listFiller = data
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_place, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return listFiller.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(listFiller[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                Timber.e("ss:$charString")
                if (charString.isEmpty()) {
                    listFiller = data
                } else {
                    val filteredList: MutableList<Post> = ArrayList()
                    for (row in data) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        val name = row.name.orEmpty()
                        Timber.e("name:$name")
                        if (name.lowercase().contains(charString.lowercase())
                        ) {
                            Timber.e("add $name")
                            filteredList.add(row)
                        }
                    }
                    listFiller = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = listFiller
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                listFiller = filterResults.values as ArrayList<Post>
                notifyDataSetChanged()
            }
        }
    }
}