package com.ictu.dltn.ui.detail

import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codepath.asynchttpclient.AsyncHttpClient
import com.codepath.asynchttpclient.RequestParams
import com.codepath.asynchttpclient.callback.TextHttpResponseHandler
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ictu.dltn.models.BaseResponse
import com.ictu.dltn.models.Post
import com.ictu.dltn.models.Rate
import com.ictu.dltn.utils.Const
import okhttp3.Headers
import timber.log.Timber
import java.lang.reflect.Type

class DetailPostViewModel : ViewModel() {
    private val _listRate = MutableLiveData<List<Rate>>()
    val listRate: LiveData<List<Rate>> = _listRate

    private val _listPostNear = MutableLiveData<List<Post>>()
    val listPostNear: LiveData<List<Post>> = _listPostNear

    val countRate = arrayListOf(0, 0, 0, 0, 0, 0)

    fun getListSize(): Int {
        return _listRate.value?.size ?: 0
    }

    fun wasRate(phone: String): Boolean {
        listRate.value?.forEach { rate ->
            if (TextUtils.equals(rate.phone, phone)) {
                return true
            }
        }
        return false
    }

    fun getRateAverage(): Double {
        val size = getListSize()
        var sum = 0.0
        if (size > 0) {

            for (i in countRate.indices) {
                countRate[i] = 0
            }

            listRate.value?.forEach { rate ->
                sum += rate.rate
                countRate[rate.rate]++
            }

            for (i in countRate.indices) {
                countRate[i] = countRate[i] * 100 / size
            }
        }

        return (sum / getListSize() * 10).toInt() / 10.0
    }

    fun getListRate(postId: Int) {
        val client = AsyncHttpClient()
        val params = RequestParams()
        client[Const.urlGetListRate(postId), params, object :
            TextHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Headers?, response: String) {
                val type: Type = object : TypeToken<BaseResponse<List<Rate>>>() {}.type
                val json = Gson().fromJson<BaseResponse<List<Rate>>>(response, type)
                _listRate.value = json.data ?: arrayListOf()
            }

            override fun onFailure(
                statusCode: Int,
                headers: Headers?,
                errorResponse: String?,
                t: Throwable?
            ) {
                Timber.e("error:${t.toString()}")
            }
        }]
    }

    fun getListPostNear(post: Post) {
        val jsonNearId = Gson().toJson(post.placeNear)
        val client = AsyncHttpClient()
        val params = RequestParams()
        client[Const.urlGetListPostNear(jsonNearId), params, object :
            TextHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Headers?, response: String) {
                val type: Type = object : TypeToken<BaseResponse<List<Post>>>() {}.type
                val json = Gson().fromJson<BaseResponse<List<Post>>>(response, type)
                _listPostNear.value = json.data ?: arrayListOf()
            }

            override fun onFailure(
                statusCode: Int,
                headers: Headers?,
                errorResponse: String?,
                t: Throwable?
            ) {
                Timber.e("error:${t.toString()}")
            }
        }]
    }
}