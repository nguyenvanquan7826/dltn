package com.ictu.dltn.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ictu.dltn.R
import com.ictu.dltn.models.Post

class PostNearAdapter(private var data: List<Post>, val onItemClick: (Post) -> Unit) :
    RecyclerView.Adapter<PostNearAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(post: Post) {
            val title = view.findViewById<TextView>(R.id.tvTitle)
            val imageView = view.findViewById<ImageView>(R.id.imageView)
            val ratingBar = view.findViewById<RatingBar>(R.id.ratingBar)

            title.text = post.name?.trim()
            ratingBar.rating = post.rateAverage.toFloat()

            Glide.with(view.context).load(post.getFirstImage()).centerCrop().into(imageView)
            view.setOnClickListener { onItemClick(post) }
        }
    }


    private var listFiller: List<Post> = arrayListOf()

    public fun setData(data: List<Post>) {
        this.data = data
        listFiller = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_place_near, parent, false)
        v.layoutParams.width = (parent.width * 0.8).toInt()
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return listFiller.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(listFiller[position])
    }
}