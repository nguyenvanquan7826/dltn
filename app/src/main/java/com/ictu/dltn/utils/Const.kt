package com.ictu.dltn.utils

public class Const {
    companion object {
        val DOMAIN = "https://dulich.cachhoc.net"

        val POST_TYPE_PLACE = 1
        val POST_TYPE_RESTAURANT = 2

        val USER_ROLE_ADMIN = 1
        val USER_ROLE_NORMAL = 2


        val KEY_DB_USER = "KEY_DB_USER"
        val URL_DEFAUL_AVATAR = "https://www.w3schools.com/howto/img_avatar.png"

        fun urlGetListPost(type: Int): String {
            return "$DOMAIN/posts.php?act=getListPost&type=$type"
        }

        fun urlGetListPostNear(jsonArrId: String): String {
            return "$DOMAIN/posts.php?act=getRestaurantNearPlace&place_near=$jsonArrId"
        }

        fun urlGetListRate(postId: Int): String {
            return "$DOMAIN/rate.php?act=getListRate&post_id=$postId"
        }

        fun urlAddRate(): String {
            return "$DOMAIN/rate.php?act=addRate"
        }

        fun urlLogin(): String {
            return "$DOMAIN/user.php?act=login"
        }
        fun urlRegister(): String {
            return "$DOMAIN/user.php?act=register"
        }
    }
}