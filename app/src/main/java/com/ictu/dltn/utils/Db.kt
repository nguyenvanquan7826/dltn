package com.ictu.dltn.utils

import android.content.Context
import com.google.gson.Gson
import com.ictu.dltn.models.User

class Db {
    companion object {
        fun getUser(context: Context): User? {
            val jsonUser = Preference().get(context).getString(Const.KEY_DB_USER, "")
            val user: User? = Gson().fromJson(jsonUser, User::class.java)
            return user
        }

        fun saveUser(context: Context, user: User?) {
            val jsonUser = Gson().toJson(user)
            Preference().save(context, Const.KEY_DB_USER, jsonUser)
        }
    }
}