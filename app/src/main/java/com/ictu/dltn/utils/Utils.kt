package com.ictu.dltn.utils

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.core.content.ContextCompat
import com.ictu.dltn.R
import top.defaults.drawabletoolbox.DrawableBuilder
import java.math.BigInteger
import java.security.MessageDigest


class Utils {
    companion object {
        fun openMap(context: Context, latitude: Double, longitude: Double) {
            val uri = "https://maps.google.com/maps?z=16&t=m&q=loc:${latitude}+${longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            context.startActivity(intent)
        }

        fun getDrawableEdt(context: Context): Drawable {
            return DrawableBuilder()
                .rectangle()
                .rounded()
                .solidColor(ContextCompat.getColor(context, R.color.bgSearch))
                .build()
        }

        fun getDrawableBtn(context: Context): Drawable {
            return DrawableBuilder()
                .rectangle()
                .rounded()
                .solidColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .solidColorPressed(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .build()
        }

        fun md5(input:String): String {
            val md = MessageDigest.getInstance("MD5")
            return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
        }
    }
}