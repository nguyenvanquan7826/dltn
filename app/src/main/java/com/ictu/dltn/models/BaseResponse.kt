package com.ictu.dltn.models

import com.google.gson.annotations.SerializedName


data class BaseResponse<T>(
    @SerializedName("status") var status: Boolean? = false,
    @SerializedName("data") var data: T? = null,
    @SerializedName("message") var message: String? = "",
)