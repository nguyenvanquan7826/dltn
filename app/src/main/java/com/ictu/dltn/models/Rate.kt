package com.ictu.dltn.models

import com.google.gson.annotations.SerializedName

data class Rate(
    @SerializedName("id") var id: Int = 0,
    @SerializedName("phone") var phone: String = "",
    @SerializedName("user_name") var userName: String = "",
    @SerializedName("post_id") var postId: Int = 0,
    @SerializedName("rate") var rate: Int = 5,
    @SerializedName("comment") var comment: String = "",
    @SerializedName("create_at") var createAt: String = "",
)
