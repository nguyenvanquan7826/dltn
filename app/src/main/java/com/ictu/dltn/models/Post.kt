package com.ictu.dltn.models

import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("id") var id: Int = 0,
    @SerializedName("name") var name: String? = null,
    @SerializedName("content") var content: String? = null,
    @SerializedName("lat") var lat: Double = 0.0,
    @SerializedName("lng") var lng: Double = 0.0,
    @SerializedName("type") var type: Int = 1,
    @SerializedName("images") var images: ArrayList<String> = arrayListOf(),
    @SerializedName("place_near") var placeNear: ArrayList<Int> = arrayListOf(),
    @SerializedName("foods") var foods: ArrayList<Int> = arrayListOf(),
    @SerializedName("address") var address: String? = null,
    @SerializedName("time_start") var timeStart: String? = null,
    @SerializedName("time_end") var timeEnd: String? = null,
    @SerializedName("rate_average") var rateAverage: Double = 0.0,
    @SerializedName("create_at") var createAt: String? = null
) : java.io.Serializable {
    fun getFirstImage(): String {
        return if (images.isEmpty()) "" else images[0]
    }
}
