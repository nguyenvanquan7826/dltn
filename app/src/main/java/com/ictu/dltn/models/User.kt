package com.ictu.dltn.models

import com.google.gson.annotations.SerializedName
import com.ictu.dltn.utils.Const

data class User(
    @SerializedName("phone") var phone: String = "",
    @SerializedName("name") var name: String = "",
    @SerializedName("pass") var pass: String = "",
    @SerializedName("role") var role: Int = Const.USER_ROLE_NORMAL,
    @SerializedName("avatar") var avatar: String = "",
    @SerializedName("token") var token: String = "",
)
